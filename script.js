function filterBy(typeofData, ...data){
    let array = [...data];
    
    if (typeofData == 'string') {
        let filter = array.filter(function(element) {
           return (typeof element !== 'string');
        });
        return filter;
    }

    else if (typeofData == 'number') {
        let filter = array.filter(function(element) {
            return (typeof element !== 'number');
        });
        return filter;
    } 

    else if (typeofData == 'null') {
        let filter = array.filter(function(element) {
            return (element !== null);
        });
        return filter;
    } 

    else if (typeofData == 'boolean') {
        let filter = array.filter(function(element) {
            return (typeof element !== 'boolean');
        });
        return filter;
    }

    else if (typeofData == 'undefined') {
        let filter = array.filter(function(element) {
            return (typeof element !== 'undefined');
        });
        return filter;
    }
    
    else if (typeofData == 'object') {
        let filter = array.filter(function(element) {
           return (typeof element !== 'object');
        });
        return filter;
    }

    return array;
}

let res = filterBy(5, true, [222, 28], null, false, 'lol', undefined, 'string', -33, 'hello world!!!', [2 , 3], undefined, null, true);

console.log(res);
